jQuery(document).ready(function() {
  window.$ = jQuery;
  var customNode = $('.mg_giving-levels:eq(1)').children();
  var placement = $('.mg_giving-levels:eq(0)').children().first();
  customNode.insertAfter(placement);
  
  var check = document.getElementById('mg_repeating_check');
  var div = check.parentNode.parentNode.parentNode;
  div.removeAttribute('class', 'col-xs-12');
  div.removeAttribute('class', 'col-xs-6');
  div.setAttribute('class', 'col-xs-1');
  var section = div.parentNode;
  var checkboxText = section.childNodes[0];
  checkboxText.removeAttribute('class', 'col-sm-3');
  checkboxText.style.paddingLeft = '10px';
  checkboxText.style.float = 'left';
  checkboxText.style.width = '80%';
  section.insertBefore(div, checkboxText);
  var node = document.createElement("p"); 
  node.setAttribute('class', 'summary-text');
  node.style.margin = '20px';
  node.style.fontSize = '15px';
  node.style.fontWeight = '600';
  var form = document.getElementsByClassName('migla-panel')[0];
  form.appendChild(node);
  toggleDedications();
  var amounts = document.getElementsByName('miglaAmount');
  for (var i = 0; i < amounts.length; i++) {
    amount = document.getElementById('miglaAmount' + i).value
    document.getElementById('miglaAmount' + i).setAttribute('data-amount', amount);
  }
  var checkbox = $('#mg_repeating_check');
  checkbox.on('change', toggleValues);
  $('input[name="miglaAmount"]').on('click change', toggleDedications);
  $('#miglaCustomAmount').on('keyup', handleCustom);
})

handleCustom = function() {
  setTimeout(function() {
    var el = document.getElementById('miglaCustomAmount');
    el.setAttribute('data-amount', el.value);
    toggleDedications();
    toggleValues();
  }, 500)
}

toggleDedications = function() {
  var selected = document.querySelector('input[name = "miglaAmount"]:checked').value;
  var two = document.getElementById('miglac_2ndDedication');
  var three = document.getElementById('miglac_3rdDedication');
  var four = document.getElementById('miglac_4thDedication');
  if (selected == 'custom') {
    value = document.getElementById('miglaCustomAmount').value;
  } else {
    value = selected;
  }
  var summaryText = '';
  if (value > 0) {
    summaryText = "You'll be donating $" + value + " to Mesorah Heritage";
    var check = document.getElementById('mg_repeating_check').checked;
    if (check) {
      summaryText += " in 5 installments";
    }
  }
  $('.summary-text').text(summaryText);
  document.getElementsByClassName('mg_Dedications')[0].parentNode.parentNode.style.display = '';
  two.parentNode.parentNode.style.display = '';
  three.parentNode.parentNode.style.display = '';
  four.parentNode.parentNode.style.display = '';
  if (value >= 18000) {
    return true;
  }
  if (value >= 5000) {
    four.value = '';
    four.parentNode.parentNode.style.display = 'none';
    return true;
  }
  if (value >= 3600) {
    three.value = '';
    three.parentNode.parentNode.style.display = 'none';
    four.value = '';
    four.parentNode.parentNode.style.display = 'none';
    return true;
  }
  if (value >= 1800) {
    four.value = '';
    four.parentNode.parentNode.style.display = 'none';   
    three.value = '';
    three.parentNode.parentNode.style.display = 'none';
    two.value = '';
    two.parentNode.parentNode.style.display = 'none';
    return true;
  }
  two.value = '';
  three.value = '';
  four.value = '';
  document.getElementsByClassName('mg_Dedications')[0].parentNode.parentNode.style.display = 'none';
  return true;
}


toggleValues = function () {
  toggleDedications();
  return true;
  var check = document.getElementById('mg_repeating_check').checked
  var amounts = document.getElementsByName('miglaAmount');
  var custom = document.getElementById('miglaCustomAmount');
  if (check) {
     for (var i = 0; i < amounts.length; i++) {
       amount = document.getElementById('miglaAmount' + i).getAttribute('data-amount')
       document.getElementById('miglaAmount' + i).value = amount / 5;
     }
     custom.value = custom.getAttribute('data-amount') / 5;
  } else {
     for (var i = 0; i < amounts.length; i++) {
       amount = document.getElementById('miglaAmount' + i).getAttribute('data-amount')
       document.getElementById('miglaAmount' + i).value = amount;
     }
     custom.value = custom.getAttribute('data-amount');
  }
  
}
