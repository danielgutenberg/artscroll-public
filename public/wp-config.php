<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'artscroll' );

/** MySQL database username */
define( 'DB_USER', 'artscroll' );

/** MySQL database password */
define( 'DB_PASSWORD', 'artscroll' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'm=h1@/Qr`GzjS^~/;g_Rubr~0T]C8xYMHp=TT1_+j3MO^9cYIh#`LjZh_Ulr8Ow+' );
define( 'SECURE_AUTH_KEY',  'fR97YPsv;,P4$D^H&jG*NH=H|OGC^q?FoIq,-ma2MKl58[CkFpO~L>FE9DB_TtH3' );
define( 'LOGGED_IN_KEY',    'FCQ(SHF{F6nFm+M.qM>Mx|t$JzxDc9ACGj xGtTws9R?[}0$&Dp$lb/XIf-o#o1A' );
define( 'NONCE_KEY',        'Rd*KXXj%[7N@GwR+%] fTd_QVwPCxL]@eFQYNhP?;0+UHoJ<d~a+u>S?yXxi~:Ds' );
define( 'AUTH_SALT',        '>oGpE5hh)/=Kt,]io=ndt=bhEDMxs|RIBqw~r82b?5#Fud~0#s.4~E{4a3~tJ69D' );
define( 'SECURE_AUTH_SALT', '1~D[6o`R5II,K++AM[YHk-jB ig>Vp+2GAKbzQ?H(/&XEpM]B[* `H-5J6G5:@}{' );
define( 'LOGGED_IN_SALT',   '{[R50$c^7h5Iu~+7M!=?|%<w753Lgx!EP1gf.HN;0hCcLR`LBa%wx,b5dvN>0x$v' );
define( 'NONCE_SALT',       'p]3!uUz= |0,uMqUY=Xyo+B(k3LLczVIPz(!i|a-fuq&IGfNW!6DrF(kbdx3{8CH' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
